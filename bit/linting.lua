local linting = {}

local go = require "plugins.bit.langs.go"
local lint = require "plugins.lintplus"

function linting.setup()
  go.setup_linter()
end

local function get_extension(filename)
  return filename:match "[^.]+$"
end

function linting.lint(the_doc)
  if get_extension(the_doc:get_name()) == "go" then
    go.format()
  end
  
  if lint.get_linter_for_doc(the_doc) ~= nil then
    lint.check(the_doc)
  end
end

return linting
