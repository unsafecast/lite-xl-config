local commands = {}

local command = require "core.command"

function commands.setup()
  command.add("core.docview", {
    ["bit:build"] = function()
      process.start { "sh", "-c", "./build.sh" }
    end,
    ["bit:run"] = function()
      process.start { "sh", "-c", "./run.sh" }
    end,
    ["bit:build-and-run"] = function()
      command.perform "bit:build"
      command.perform "bit:run"
    end,
  })
end

return commands
