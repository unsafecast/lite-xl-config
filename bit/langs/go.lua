local go = {}

local lint = require "plugins.lintplus"
local command = require "core.command"

function go.format()
  command.perform "gofmt:gofmt"
  command.perform "gofmt:goimports"
end

function go.setup_linter()
  local command = lint.args_command({
    "go",
    "vet",
    lint.filename
  }, "go_args")
  
  lint.add("go") {
    filename = "%.go$",
    procedure = {
      command = command,
      interpreter = lint.interpreter {
        error = "vet: (.-):(%d+):(%d+): (.+)"
      }
    }
  }
end

return go
