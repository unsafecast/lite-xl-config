local keys = {}

local keymap = require "core.keymap"

keymap.add {
  ["ctrl+m"] = "bit:go-build-and-run",
}

return keys
