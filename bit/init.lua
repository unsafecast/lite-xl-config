-- mod-version:2 -- lite-xl 2.0

local config = require "core.config"

local linting = require "plugins.bit.linting"
linting.setup()

local commands = require "plugins.bit.commands"
commands.setup()

local hooks = require "plugins.bit.hooks"
hooks.setup()

local keys = require "plugins.bit.keys"
hooks.setup() 

