local hooks = {}

local Doc = require "core.doc"
local linting = require "plugins.bit.linting"

local function install_save_hook()
  local old_save = Doc.save
  
  function Doc:save(filename, abs_filename)
    old_save(self, filename, abs_filename)
    linting.lint(self)
    old_save(self, filename, abs_filename)
  end
end

function hooks.setup()
  install_save_hook()
end

return hooks
